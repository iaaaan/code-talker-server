
function protectedRoute (req, res, next) {
  if (req.isAuthenticated()) {
    return next()
  }

  res.json({ status: 'error', error: 'user needs to sign in' })
  // req.session.returnTo = req.originalUrl
  // res.redirect('/signin')
}

module.exports = protectedRoute
