// import modules
const db = require('../lib/db')
const passport = require('passport')
const protectedRoute = require('../authenticate/protected-route')
const pass = require('pwd')
const { check, validationResult } = require('express-validator')

// const axios = require('axios')

// export module
module.exports = users

function users (app) {
  app.get('/signin', function (req, res) {
    return res.render('signin')
  })

  app.post(
    '/signin',
    [
      check('username').trim().escape(),
      check('password').trim().escape()
    ],
    (req, res, next) => {
      const errors = validationResult(req)
      if (!errors.isEmpty()) {
        return res.json({ status: 'error', error: errors })
      }

      passport.authenticate('local', (err, user, info) => {
        if (err) {
          return res.json({ status: 'error', error: err })
        }

        if (!user) {
          return res.json({ status: 'error', error: 'Wrong username or password' })
        }

        req.logIn(user, (err) => {
          if (err) {
            return res.json({ status: 'error', error: err })
          }

          return res.json({ status: 'success', data: { username: req.user } })
        })
      })(req, res, next)
    }
  )

  app.post(
    '/signup',
    [
      check('username').trim().escape(),
      check('email').isEmail().trim().escape(),
      check('password').trim().escape(),
      check('verification').trim().escape()
      // check('g-recaptcha-response').trim().escape()
    ],
    (req, res, next) => {
      var username = req.body.username
      var email = req.body.email
      var password = req.body.password
      var verification = req.body.verification

      // var recaptchaResponse = req.body['g-recaptcha-response']

      const errors = validationResult(req)
      if (!errors.isEmpty()) {
        return res.json({ status: 'error', error: errors })
      }

      // if (!recaptchaResponse) {
      //   return res.redirect('/signin#signup')
      // }

      // axios
      //   .post(
      //     `https://www.google.com/recaptcha/api/siteverify?secret=${process.env.RECAPTCHA_SECRET}&response=${recaptchaResponse}`,
      //     {},
      //     {
      //       headers: {
      //         'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'
      //       }
      //     }
      //   )
      //   .then((res) => {
      //     if (!res.data.success) {
      //       req.flash('error', 'Internal server error, please try again')
      //       return res.redirect('/signin#signup')
      //     }

      let error = null
      const EMAIL_REGEXP = /^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,6}$/

      // check for valid inputs
      if (!username || !email || !password || !verification) {
        error = 'All fields are required'
      } else if (username !== encodeURIComponent(username)) {
        error = 'Username may not contain any non-url-safe characters'
      } else if (!email.match(EMAIL_REGEXP)) {
        error = 'Email is invalid'
      } else if (password !== verification) {
        error = 'Passwords don\'t match'
      }

      if (error) {
        return res.json({ status: 'error', error })
      }

      // check if username is already taken
      db.get(
        `user!${username}:username`,
        (err, value) => {
          if (!err || value) {
            return res.json({ status: 'error', error: 'Username already exists' })
          }

          // create salt and hash password
          pass.hash(password, function (err, salt, hash) {
            if (err) console.log(err)

            var user = {
              username: username,
              email: email,
              salt: salt,
              hash: hash,
              createdAt: Date.now()
            }

            var createUser = [
              { type: 'put', key: `user!${user.username}:username`, value: user.username },
              { type: 'put', key: `user!${user.username}:email`, value: user.email },
              { type: 'put', key: `user!${user.username}:pwdsalt`, value: user.salt },
              { type: 'put', key: `user!${user.username}:pwdhash`, value: user.hash },
              { type: 'put', key: `user!${user.username}:createdAt`, value: user.createdAt },
              { type: 'put', key: `user!${user.username}:friends`, value: [] },
              { type: 'put', key: `user!${user.username}:verified`, value: [] },
              { type: 'put', key: `user!${user.username}:requests`, value: [] },
              { type: 'put', key: `user!${user.username}:updateNeeded`, value: true }
            ]

            db.batch(createUser, function (err) {
              if (err) {
                return res.json({ status: 'error', error: err })
              }

              req.body.username = username
              req.body.password = password
              return next()
            })
          })
        }
      )
      // })
      // .catch((error) => {
      //   return res.json({ status: 'error', error })
      // })
    },
    (req, res, next) => {
      passport.authenticate('local', (err, user, info) => {
        if (err) {
          return res.json({ status: 'error', error: err })
        }

        if (!user) {
          return res.json({ status: 'error', error: 'Wrong username or password' })
        }

        req.logIn(user, (err) => {
          if (err) {
            return next(err)
          }

          return res.json({ status: 'success', data: { username: req.user } })
        })
      })(req, res, next)
    }
  )

  app.get('/me', protectedRoute, (req, res) => {
    res.json({ status: 'success', data: { username: req.user } })
  })

  app.get('/signout', protectedRoute, (req, res) => {
    req.session.destroy()
    res.json({ status: 'success' })
  })
}
