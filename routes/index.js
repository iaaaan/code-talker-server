// import route functinos
// const assignments = require('./assignments')
// const tasks = require('./tasks')
const db = require('../lib/db')
const protectedRoute = require('../authenticate/protected-route')
const users = require('./users')
const crypto = require('../lib/crypto.js')

function routes (app) {
  users(app)

  // home route
  app.get('/', (req, res) => {
    return res.json({ status: 'success', data: 'ok' })
  })

  app.post('/encrypt', protectedRoute, (req, res) => {
    const {
      plainText
    } = req.body

    crypto.encrypt(plainText, req.user, (error, codeTalkerMessage) => {
      if (error) {
        return res.json({ status: 'error', error })
      }

      return res.json({ status: 'success', data: { codeTalkerMessage } })
    })
  })

  app.post('/decrypt', protectedRoute, (req, res) => {
    const {
      codeTalkerMessage
    } = req.body

    crypto.decrypt(codeTalkerMessage, req.user, (error, plainText) => {
      if (error) {
        return res.json({ status: 'error', error })
      }

      return res.json({ status: 'success', data: { plainText } })
    })
  })

  app.post('/request', protectedRoute, (req, res) => {
    const {
      username
    } = req.body

    const requestsKey = `user!${username}:requests`
    db.get(requestsKey, (error, requests) => {
      if (error) {
        return res.json({ status: 'error', error })
      }

      if (requests.indexOf(req.user) === -1) {
        requests.push(req.user)
        db.put(requestsKey, requests, error => {
          if (error) {
            return res.json({ status: 'error', error })
          }

          return res.json({ status: 'success' })
        })
      } else {
        return res.json({ status: 'error', error: 'request was already sent' })
      }
    })
  })

  app.post('/delete', protectedRoute, (req, res) => {
    const {
      username
    } = req.body

    const userFriendsKey = `user!${req.user}:friends`
    db.get(userFriendsKey, (error, friends) => {
      if (error) {
        console.error(error) 
        return res.json({ status: 'error', error })
      }

      const updatedFriends = friends.filter(friend => friend !== username)
      db.put(userFriendsKey, updatedFriends, (error) => {
        if (error) {
          console.error(error)
          return res.json({ status: 'error', error })
        }

        const userUpdateKey = `user!${req.user}:updateNeeded`
        db.put(userUpdateKey, true, (error) => {
          if (error) {
            console.error(error)
            return res.json({ status: 'error', error })
          }

          const friendsFriendsKey = `user!${username}:friends`
          db.get(friendsFriendsKey, (error, friends) => {
            if (error) {
              console.error(error)
              return res.json({ status: 'error', error })
            }

            const updatedFriendsFriends = friends.filter(friendsFriend => friendsFriend !== req.user)

            db.put(friendsFriendsKey, updatedFriendsFriends, (error) => {
              if (error) {
                console.error(error)
                return res.json({ status: 'error', error })
              }

              const friendUpdateKey = `user!${username}:updateNeeded`
              db.put(friendUpdateKey, true, (error) => {
                if (error) {
                  console.error(error)
                  return res.json({ status: 'error', error })
                }

                res.json({ status: 'success', data: { friends: updatedFriends } })
              })
            })
          })
        })
      })
    })
  })

  app.post('/accept', protectedRoute, (req, res) => {
    const {
      username
    } = req.body

    const requestsKey = `user!${req.user}:requests`
    db.get(requestsKey, (error, requests) => {
      if (error) {
        return res.json({ status: 'error', error })
      }

      const requestId = requests.indexOf(username)
      if (requestId > -1) {
        const friendsKey = `user!${req.user}:friends`
        db.get(friendsKey, (error, friends) => {
          if (error) {
            return res.json({ status: 'error', error })
          }

          if (friends.indexOf(username) > -1) {
            return res.json({ status: 'error', error: 'user is already a friend' })
          } else {
            friends.push(username)

            db.put(friendsKey, friends, (error) => {
              if (error) {
                return res.json({ status: 'error', error })
              }

              const targetFriendsKey = `user!${username}:friends`
              db.get(targetFriendsKey, (error, targetFriends) => {
                if (error) {
                  return res.json({ status: 'error', error })
                }

                targetFriends.push(req.user)

                db.put(targetFriendsKey, targetFriends, (error) => {
                  if (error) {
                    return res.json({ status: 'error', error })
                  }

                  db.put(`user!${req.user}:updateNeeded`, true)
                  db.put(`user!${username}:updateNeeded`, true)
                  requests.splice(requestId, 1)
                  db.put(requestsKey, requests, (error) => {
                    if (error) {
                      console.error(error)
                    }
                  })

                  return res.json({ status: 'success', data: { friends, requests } })
                })
              })
            })
          }
        })
      } else {
        return res.json({ status: 'error', error: `there's no request from this user` })
      }
    })
  })

  app.post('/reject', protectedRoute, (req, res) => {
    const {
      username
    } = req.body

    const requestsKey = `user!${req.user}:requests`
    db.get(requestsKey, (error, requests) => {
      if (error) {
        return res.json({ status: 'error', error })
      }

      const requestId = requests.indexOf(username)
      if (requestId === -1) {
        return res.json({ status: 'error', error: `there's no request from this user` })
      } else {
        requests.splice(requestId, 1)
        db.put(requestsKey, requests, (error) => {
          if (error) {
            console.error(error)
          }

          return res.json({ status: 'success', data: { requests } })
        })
      }
    })
  })

  app.post('/verify', protectedRoute, (req, res) => {
    const {
      username
    } = req.body

    const friendsKey = `user!${req.user}:friends`
    db.get(friendsKey, (error, friends) => {
      if (error) {
        return res.json({ status: 'error', error: error })
      }

      if (friends.indexOf(username) === -1) {
        return res.json({ status: 'error', error: 'user is not a friend' })
      } else {
        const verifiedKey = `user!${req.user}:verified`
        db.get(verifiedKey, (error, verified) => {
          if (error) {
            return res.json({ status: 'error', error: error })
          }

          if (verified.indexOf(username) > -1) {
            return res.json({ status: 'error', error: 'user is already verified' })
          }

          verified.push(username)
          db.put(verifiedKey, verified, error => {
            if (error) {
              return res.json({ status: 'error', error })
            }

            res.json({ status: 'success', data: { verified } })
          })
        })
      }
    })
  })

  app.get('/friends', protectedRoute, (req, res) => {
    const friendsKey = `user!${req.user}:friends`
    db.get(friendsKey, (error, friends) => {
      if (error) {
        return res.json({ status: 'error', error: error })
      }

      res.json({ status: 'success', data: { friends } })
    })
  })

  app.get('/requests', protectedRoute, (req, res) => {
    const requestsKey = `user!${req.user}:requests`
    db.get(requestsKey, (error, requests) => {
      if (error) {
        return res.json({ status: 'error', error: error })
      }

      res.json({ status: 'success', data: { requests } })
    })
  })

  app.get('/verified', protectedRoute, (req, res) => {
    const verifiedKey = `user!${req.user}:verified`
    db.get(verifiedKey, (error, verified) => {
      if (error) {
        return res.json({ status: 'error', error: error })
      }

      res.json({ status: 'success', data: { verified } })
    })
  })

  // 404 not found route
  app.all('*', function (req, res) {
    return res.json({ status: 'error', error: 404 })
  })
}

module.exports = routes
