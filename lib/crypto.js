
const cpabe = require('node-cp-abe')
const db = require('../lib/db')
const shortid = require('shortid')

const {
  mstkey,
  pubkey
} = cpabe.setup()

function updatePrivateKey (username, callback) {
  db.get(`user!${username}:friends`, (error, friends) => {
    if (error) {
      return callback(error)
    }

    db.get(`user!${username}:verified`, (error, verified) => {
      if (error) {
        return callback(error)
      }

      const privileges = friends
        .filter(friend => verified.indexOf(friend) === -1)
        .map(friend => `${friend}`)
        .concat([`${username}`])
        // .concat(
        //   verified
        //     .map(v => `${v}`)
        // )

      console.log('new privileges', privileges)
      try {
        const privateKey = cpabe.keygen(pubkey, mstkey, privileges)
        callback(null, privateKey)
      } catch (error) {
        callback(error)
      }
    })
  })
}

function encrypt (plainText, username, callback) {
  try {
    const id = shortid.generate().toLowerCase()
    const policy = `${username}`
    // const policy = `ian`
    const cipherBuffer = cpabe.encryptMessage(pubkey, policy, Buffer.from(plainText))
    const cipherText = Buffer.from(cipherBuffer).toString('base64')
    console.log('new encryption: ', cipherText)
    const codeTalkerMessage = `${cipherText};;;${username};;;0;;;${id}`
    callback(null, codeTalkerMessage)
  } catch (error) {
    callback(error)
  }
}

function decrypt (codeTalkerMessage, username, callback) {
  const [
    cipherText,
    contact,
    verified
  ] = codeTalkerMessage.split(';;;')

  console.log('1', codeTalkerMessage)

  const contactsKey = `user!${username}:${!parseInt(verified) ? 'friends' : 'verified'}`

  db.get(contactsKey, (error, contacts) => {
    if (error) {
      return callback(error)
    }

    if (contacts.indexOf(contact) === -1) {
      const error = `user doesn't have access to this message`
      return callback(error)
    }

    console.log('2')
    db.get(`user!${username}:updateNeeded`, (error, updateNeeded) => {
      if (error) {
        return callback(error)
      }

      // TODO
      console.log('3')
      if (true) {
      // if (updateNeeded) {
        console.log('4')
        updatePrivateKey(username, (error, privateKey) => {
          if (error) {
            return callback(error)
          }

          try {
            const cipherBuffer = Buffer.from(cipherText, 'base64')
            console.log('OKOKOK ', pubkey)
            console.log('OKOKOK ', privateKey)
            console.log('OKOKOK ', cipherBuffer)
            console.log('OKOKOK ', cipherText)
            const plainText = cpabe.decryptMessage(pubkey, privateKey, cipherBuffer).toString()
            console.log('6')
            callback(null, plainText)
          } catch (error) {
            console.log(error)
            callback(error)
          }
        })
      } else {
        console.log('7')
        db.get(`user!${username}:privateKey`, (error, privateKey) => {
          if (error) {
            return callback(error)
          }

          try {
            console.log('8')
            const plainText = cpabe.decryptMessage(pubkey, privateKey, cipherText).toString()
            console.log('9')
            callback(null, plainText)
          } catch (error) {
            callback(error)
          }
        })
      }
    })
  })
}

module.exports = {
  updatePrivateKey,
  encrypt,
  decrypt
}
